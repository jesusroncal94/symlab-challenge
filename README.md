# Symlab Challenge!

# Building Project

## Building the services for the first time in local
To build the services with `docker` and `docker-compose` execute the command: `docker-compose up --build`.

The `symlab-challenge_app` service trying connecting to the `symlab-challenge_db` service until it is ready to accept connections and then the application will create the tables in `symlab_db`.

## Getting up the services
To up the project execute the command: `docker-compose up`.

## Getting info about the services
To up the project execute the command: `docker-compose ps`. It will show you info similar to this:
Name|Command|State|Ports
--|--|--|--
symlab-challenge_app|python manage.py|Up|0.0.0.0:5000->5000/tcp,:::5000->5000/tcp
symlab-challenge_db|docker-entrypoint.sh postgres|Up|0.0.0.0:15432->5432/tcp,:::15432->5432/tcp

# Interacting with Project

## Endpoints
The base uri of API depends on environment:
 1. *Local*: {"api": "http://localhost:5000/api/v1"}
 2. *Production*: {"api": "\<ip or domain\>/api/v1"}

### API Index
`{api}/`:
- Main page of API. GET Method.

### Mechanical Workshop
`{api}/mechanicalworkshops/`:
- List of mechanical workshops. GET Method.
- Create a mechanical workshop. POST Method. 

`{api}/mechanicalworkshops/{mechanicalWorkshopId}`:
- Get a mechanical workshop by id. GET Method.

### Customer
`{api}/customers/`:
- List of customers. GET Method.
- Create a customer. POST Method. 

`{api}/customers/{customerId}`:
- Get a customer by id. GET Method.

### Vehicles
`{api}/vehicles/`:
- List of vehicles. GET Method.
- Create a vehicle. POST Method. 

`{api}/vehicles/{customerId}`:
- Get a vehicle by id. GET Method.

### Repairs
`{api}/repairs/`:
- List of repairs. GET Method.
- Create a repair. POST Method. 

`{api}/repairs/{customerId}`:
- Get a repair by id. GET Method.

## Pagination
You can add in querystring the params `page (default=1)` and `per_page (default=10)` in `list endpoints`. Example:
- `{api}/mechanicalworkshops/?page=1&per_page=2`.

## Filtering
You can add in querystring the fields of the entities (tables) as params to filter the searching in `list endpoints`. Example:
- `{api}/mechanicalworkshops/?code=MW1`.

## Sorting
You can add in querystring the key `sort` and then a field of the entities (table) as param to sort the searching in `list endpoints`. If you need to sort in descending order, just add the minus symbol in front of the field.  Examples:
- Ascending order`{api}/mechanicalworkshops/?sort=created`.
- Descending order`{api}/mechanicalworkshops/?sort=-created`.

## Testing
To run tests execute the command: `docker exec -it symlab-challenge_app pytest -v`.

# Database modeling
- For a better interaction with the application, the entity diagram with their respective and described fields is shown below:
![](docs/db/Symlab-Challenge_Entity-Diagram.png)
- For more information about collections you can check the file exported from Insomnia: [symlab_collection.json](symlab_collection.json).