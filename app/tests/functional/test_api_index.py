def test_api_index(test_client):
    response = test_client.get('/api/v1/')
    response_dict = response.get_json()

    assert response.status_code == 200
    assert response_dict.get('data') == 'Flask API running successfully!'
