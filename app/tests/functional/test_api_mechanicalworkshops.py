import uuid

from api.src.models import db
from api.src.models.mechanicalworkshops import MechanicalWorkshop


uuid_for_testing = uuid.uuid4()


def test_api_structure_of_list_mechanical_workshops(test_client):
    response = test_client.get('/api/v1/mechanicalworkshops/')
    response_dict = response.get_json()

    assert response.status_code == 200
    assert 'data' in response_dict.keys()
    assert 'metadata' in response_dict.keys()


def test_api_create_mechanical_workshop(test_client):
    # Create Mechanical Workshop
    mechanical_workshop = MechanicalWorkshop(**{
        'id': str(uuid_for_testing),
        'code': f'MW{str(uuid_for_testing)[0:8]}',
        'name': f'Mechanical Workshop {str(uuid_for_testing)[0:8]}',
        'description': f'Mechanical Workshop {str(uuid_for_testing)[0:8]}',
        'address': '123 Main ST CA',
        'phone': '+1 123456789',
        'email': 'mechanical_workshop@example.com'
    })
    db.session.add(mechanical_workshop)
    db.session.commit()

    # Validate
    response = test_client.get(f'/api/v1/mechanicalworkshops/{str(uuid_for_testing)}')
    response_dict = response.get_json()
    data = response_dict.get('data', {})

    assert data.get('id') == str(uuid_for_testing)
    assert data.get('code') == f'MW{str(uuid_for_testing)[0:8]}'
    assert data.get('name') == f'Mechanical Workshop {str(uuid_for_testing)[0:8]}'
    assert data.get('description') == f'Mechanical Workshop {str(uuid_for_testing)[0:8]}'
    assert data.get('address') == '123 Main ST CA'
    assert data.get('phone') == '+1 123456789'
    assert data.get('email') == 'mechanical_workshop@example.com'
    assert data.get('customers') == []
