from api.src.controllers import BaseAPI
from api.src.models.mechanicalworkshops import MechanicalWorkshop
from api.src.schemas.mechanicalworkshops import MechanicalWorkshopSchema


class MechanicalWorkshopAPI(BaseAPI):
    model = MechanicalWorkshop
    schema = MechanicalWorkshopSchema()
