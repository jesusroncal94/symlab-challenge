from flask import jsonify, request
from flask.views import MethodView
from six import string_types
from werkzeug.exceptions import BadRequest, MethodNotAllowed, NotFound

from api.src.models import db
from config import Config


class BaseAPI(MethodView):
    config = Config()
    model = None
    schema = None
    session = db.session

    def get(self, **kwargs):
        try:
            # Initialize variables
            tablename = self.model.__tablename__
            args = dict(request.args)
            identifiers, filters = {}, {}
            _sort, sort_data = args.pop('sort', []), []
 
            # Get pagination config
            page = int(args.pop('page', self.config.DEFAULT_CURRENT_PAGE))
            per_page = int(args.pop('per_page', self.config.DEFAULT_PER_PAGE_SIZE))
 
            # Get identifiers
            for key, value in kwargs.items():
                if key == tablename[:1].lower() + tablename[1:] + 'Id':
                    if value:
                        identifiers.update({
                            'id': value
                        })
                elif key == 'id' or getattr(self.model, key):
                    identifiers.update({
                        key: value
                    })
            # Apply identifiers
            query = self.model.query.filter_by(**identifiers)

            if not identifiers.get('id'):
                # Return a list of elements
                # Apply filters
                for key, value in args.items():
                    if getattr(self.model, key):
                        filters.update({
                            key: value
                        })
                # Apply order
                if _sort:
                    _sort = [_sort] if isinstance(_sort, string_types) else _sort
                    for s in _sort:
                        if s[0] == '-':
                            sort_data.append(getattr(self.model, s[1:]).desc())
                        else:
                            sort_data.append(getattr(self.model, s))

                # Search all elements and paginate
                result = query.filter_by(**filters).order_by(*sort_data).paginate(
                    page=page, per_page=per_page
                )

                # Serialize
                self.schema.many = True
                return jsonify(
                    data=self.schema.dump(result.items),
                    metadata={
                        'page': result.page,
                        'pages': result.pages,
                        'total': result.total,
                        'prev_num': result.prev_num,
                        'next_num': result.next_num,
                        'has_next': result.has_next,
                        'has_prev': result.has_prev
                    })
            else:
                # Expose a single element
                # Search and validate element by id
                result = query.first()
                if not result:
                    raise NotFound
                self.schema.many = False
                return jsonify(data=self.schema.dump(result))
        except Exception as exc:
            raise BadRequest(f"{exc.__class__.__name__} Exception: [{exc}]")

    def post(self, **kwargs):
        try:
            # Initialize variables
            data = request.json
            self.schema.many = False
            # # Update data with related identifiers
            # for key, value in kwargs.items():
            #     if getattr(self.model, key):
            #         data.update({
            #             key: value
            #         })

            # Validate and load data
            validated_data = self.schema.load(data)

            # Create element
            self.session.add(validated_data)
            self.session.commit()
            created = self.schema.dump(validated_data)

            return jsonify(data=created)
        except Exception as exc:
            raise BadRequest(f"{exc.__class__.__name__} Exception: [{exc}]")

    def put(self, **kwargs):
        raise MethodNotAllowed

    def delete(self, **kwargs):
        raise MethodNotAllowed
