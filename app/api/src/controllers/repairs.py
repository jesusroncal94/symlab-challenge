from api.src.controllers import BaseAPI
from api.src.models.repairs import Repair
from api.src.schemas.repairs import RepairSchema


class RepairAPI(BaseAPI):
    model = Repair
    schema = RepairSchema()
