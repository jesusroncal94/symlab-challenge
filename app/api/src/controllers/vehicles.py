from api.src.controllers import BaseAPI
from api.src.models.vehicles import Vehicle
from api.src.schemas.vehicles import VehicleSchema


class VehicleAPI(BaseAPI):
    model = Vehicle
    schema = VehicleSchema()
