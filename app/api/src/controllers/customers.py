from api.src.controllers import BaseAPI
from api.src.models.customers import Customer
from api.src.schemas.customers import CustomerSchema


class CustomerAPI(BaseAPI):
    model = Customer
    schema = CustomerSchema()
