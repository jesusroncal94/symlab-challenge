from flask import Blueprint, jsonify


api_blueprint = Blueprint('api_blueprint', __name__, url_prefix='/api/v1')


@api_blueprint.route('/')
def index():
    return jsonify(data='Flask API running successfully!')


def register_api(view, endpoint, url, pk='id', pk_type='int'):
    view_func = view.as_view(endpoint)
    api_blueprint.add_url_rule(url, defaults={pk: None},
                     view_func=view_func, methods=['GET',])
    api_blueprint.add_url_rule(url, view_func=view_func, methods=['POST',])
    api_blueprint.add_url_rule(f'{url}<{pk_type}:{pk}>', view_func=view_func,
                     methods=['GET', 'PUT', 'DELETE'])


from api.src.routes import customers as _
from api.src.routes import mechanicalworkshops as _
from api.src.routes import repairs as _
from api.src.routes import vehicles as _
