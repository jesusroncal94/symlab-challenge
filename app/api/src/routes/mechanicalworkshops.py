from api.src.controllers.mechanicalworkshops import MechanicalWorkshopAPI
from api.src.routes import register_api


register_api(MechanicalWorkshopAPI, 'mechanical_workshop_api', '/mechanicalworkshops/', pk='mechanicalWorkshopId', pk_type='uuid')
