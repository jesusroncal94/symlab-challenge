from api.src.controllers.vehicles import VehicleAPI
from api.src.routes import register_api


register_api(VehicleAPI, 'vehicle_api', '/vehicles/', pk='vehicleId', pk_type='uuid')
