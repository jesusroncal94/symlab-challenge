from api.src.controllers.customers import CustomerAPI
from api.src.routes import register_api


# register_api(CustomerAPI, 'customer_api', '/mechanicalworkshops/<uuid:mechanicalWorkshopId>/customers/', pk='customerId', pk_type='uuid')
register_api(CustomerAPI, 'customer_api', '/customers/', pk='customerId', pk_type='uuid')
