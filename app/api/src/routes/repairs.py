from api.src.controllers.repairs import RepairAPI
from api.src.routes import register_api


register_api(RepairAPI, 'repair_api', '/repairs/', pk='repairId', pk_type='uuid')
