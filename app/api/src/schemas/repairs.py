from marshmallow_sqlalchemy.fields import Nested

from api.src.models.repairs import Repair
from api.src.schemas import ma


class RepairSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Repair
        include_fk = True
        include_relationships = True
        load_instance = True

    vehicle = mechanicalWorkshop = Nested('VehicleSchema', exclude=['repairs'])
