from api import ma


from api.src.schemas.customers import CustomerSchema as _
from api.src.schemas.mechanicalworkshops import MechanicalWorkshopSchema as _
from api.src.schemas.repairs import RepairSchema as _
from api.src.schemas.vehicles import VehicleSchema as _
