from marshmallow_sqlalchemy.fields import Nested

from api.src.models.customers import Customer
from api.src.schemas import ma


class CustomerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Customer
        include_fk = True
        include_relationships = True
        load_instance = True

    mechanicalWorkshop = Nested('MechanicalWorkshopSchema', exclude=['customers'])
    vehicles = Nested('VehicleSchema', many=True, exclude=['customer'])
