from marshmallow_sqlalchemy.fields import Nested

from api.src.models.mechanicalworkshops import MechanicalWorkshop
from api.src.schemas import ma


class MechanicalWorkshopSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = MechanicalWorkshop
        include_fk = True
        include_relationships = True
        load_instance = True

    customers = Nested('CustomerSchema', many=True, exclude=['mechanicalWorkshop'])
