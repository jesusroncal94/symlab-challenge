from marshmallow_sqlalchemy.fields import Nested

from api.src.models.vehicles import Vehicle
from api.src.schemas import ma


class VehicleSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Vehicle
        include_fk = True
        include_relationships = True
        load_instance = True

    customer = mechanicalWorkshop = Nested('CustomerSchema', exclude=['vehicles'])
    repairs = Nested('RepairSchema', many=True, exclude=['vehicle'])
