import uuid
from sqlalchemy.dialects.postgresql import UUID

from api.src.models import db, TimestampMixin


class Vehicle(TimestampMixin, db.Model):
    __tablename__ = 'Vehicle'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    registerNumber = db.Column(db.String(20), unique=True, nullable=False)
    description = db.Column(db.String(250), nullable=False)
    brand = db.Column(db.String(100), nullable=False)
    model = db.Column(db.String(50), nullable=False)
    fuelUsed = db.Column(db.String(50), nullable=False)
    type = db.Column(db.String(50), nullable=False)
    engineCc = db.Column(db.Integer, nullable=False)
    customerId = db.Column(UUID(as_uuid=True), db.ForeignKey('Customer.id'), nullable=False)

    customer = db.relationship('Customer', back_populates='vehicles')
    repairs = db.relationship('Repair', back_populates='vehicle')

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id} | {self.registerNumber}>'
