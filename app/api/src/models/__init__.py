from datetime import datetime

from api import db


class TimestampMixin():
    created = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated = db.Column(db.DateTime, onupdate=datetime.now)


from api.src.models.customers import Customer as _
from api.src.models.mechanicalworkshops import MechanicalWorkshop as _
from api.src.models.repairs import Repair as _
from api.src.models.vehicles import Vehicle as _
