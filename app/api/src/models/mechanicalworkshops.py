import uuid
from sqlalchemy.dialects.postgresql import UUID

from api.src.models import db, TimestampMixin


class MechanicalWorkshop(TimestampMixin, db.Model):
    __tablename__ = 'MechanicalWorkshop'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    code = db.Column(db.String(10), unique=True, nullable=False)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(250), nullable=False)
    address = db.Column(db.String(250), nullable=False)
    phone = db.Column(db.String(15), nullable=False)
    email = db.Column(db.String(250), nullable=False)

    customers = db.relationship('Customer', back_populates='mechanicalWorkshop')

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id} | {self.code} | {self.name}>'
