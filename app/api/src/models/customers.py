import uuid
from sqlalchemy.dialects.postgresql import UUID

from api.src.models import db, TimestampMixin


class Customer(TimestampMixin, db.Model):
    __tablename__ = 'Customer'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    document = db.Column(db.String(15), unique=True, nullable=False)
    phone = db.Column(db.String(15), nullable=False)
    email = db.Column(db.String(250), unique=True, nullable=False)
    address = db.Column(db.String(250), nullable=False)
    mechanicalWorkshopId = db.Column(UUID(as_uuid=True), db.ForeignKey('MechanicalWorkshop.id'), nullable=False)

    mechanicalWorkshop = db.relationship('MechanicalWorkshop', back_populates='customers')
    vehicles = db.relationship('Vehicle', back_populates='customer')

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id} | {self.name} {self.lastname}>'
