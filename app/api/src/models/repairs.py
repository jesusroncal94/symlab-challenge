import uuid
from sqlalchemy.dialects.postgresql import UUID

from api.src.models import db, TimestampMixin


class Repair(TimestampMixin, db.Model):
    __tablename__ = 'Repair'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    description = db.Column(db.String(250), nullable=False)
    observation = db.Column(db.String(250), nullable=False)
    mount = db.Column(db.Float, nullable=False)
    vehicleId = db.Column(UUID(as_uuid=True), db.ForeignKey('Vehicle.id'), nullable=False)

    vehicle = db.relationship('Vehicle', back_populates='repairs')

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.id}>'
