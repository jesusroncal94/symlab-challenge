from flask import Flask
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

from config import Config


db = SQLAlchemy()
ma = Marshmallow()


def create_app() -> Flask:
    app = Flask(__name__)
    app.config.from_object(Config)
    _ = CORS(app, resources={r"/api/*": {"origins": "*"}})

    initialize_extensions(app)
    register_blueprints(app)

    return app


def initialize_extensions(app: Flask) -> None:
    db.init_app(app)
    ma.init_app(app)

    with app.app_context():
        from api.src import models as _
        db.create_all()


def register_blueprints(app: Flask) -> None:
    from api.src.routes import api_blueprint
    app.register_blueprint(api_blueprint)
